#!/usr/bin/python3
#
# install.py
#
# simple Linux installer for pg_ssl_init;
# sets up bin links in /usr/local/sbin, for admins...
#

import sys
import errno
import os
import stat
import shutil

SOURCE_FILES=['pg_ssl_init_server.py', 'pg_ssl_init_client.py']
SHARE_DIR='/usr/share'
TARGET_DIR=SHARE_DIR+'/pg_ssl_init'
BIN_DIR='/usr/local/sbin' # administrative...
BIN_HACK='/usr/bin/python3'

def err (message=''):
    sys.stderr.write (message+'\n') # err

def plural(count):
    if count==1:
       return '' # plural

    return 's' # plural

def mkdir(path):
    if not os.path.isdir(path):
       os.mkdir (path)

def rm(path):
    if os.path.isfile(path):
       os.remove (path)

def chmod_x(path):
    os.chmod (path, os.stat(path).st_mode|stat.S_IEXEC)

def filename(path):
    return os.path.splitext(path)[0] # filename

def nix(): # Linux, OSx, or BSD -not Windows...
    return os.name!='nt' # nix

if not nix():
   err ('This script is for Linux/Unix only')
   err ('(contact the developer if you need one for Windows...)')
   exit (errno.ENOSYS) # Function not implemented...

if not os.path.isdir(SHARE_DIR):
   err ('Standard Unix path not found: '+SHARE_DIR)
   err ('Exiting...')
   exit (errno.ENOENT) # No such file or directory...

if not os.path.isdir(BIN_DIR):
   err ('Standard Unix [admin] path not found: '+BIN_DIR)
   err ('Exiting...')
   exit (errno.ENOENT) # No such file or directory...

if not os.path.isfile(BIN_HACK):
   err ('Python interpreter path not found: '+BIN_HACK)
   err ('Exiting...')
   exit (errno.ENOENT) # No such file or directory...

source_dir=os.path.dirname(sys.argv[0])
if source_dir=='':
   source_dir='.'

missing=[]
for py in SOURCE_FILES:
    if not os.path.isfile(source_dir+'/'+py):
       missing+=[py]

if len(missing)>0:
   err ('Missing the following file'+plural(len(missing))+': '+','.join(missing))
   exit (errno.ENOENT) # No such file or directory

err ('Installed the following:')
for py in SOURCE_FILES:
    source_path=os.path.abspath(source_dir+'/'+py)
    target_path=os.path.abspath(TARGET_DIR+'/'+py)
    if source_path!=target_path:
       shutil.copyfile (source_path, target_path)

    chmod_x (target_path)
    bin_path=BIN_DIR+'/'+filename(py)
    if os.path.isfile(bin_path):
       rm (bin_path)

    os.symlink (target_path, bin_path)
    err ('\t'+bin_path)
