#!/usr/bin/python3
#
# pg_ssl_init_client.py
#
# pg_ssl_init_client [postgres_user_list]
#
# generates client keys for SSL access (typically for pgadmin...)
# in an "ssl" subfolder of the postgres configuration files
#
# prerequisite external commands: psql, openssl
#

VERSION='12.2.12' # based on testing with postgres 12.2...
COMPANION_SCRIPT='pg_ssl_init_server'
TESTED_PYTHON_VERSION=(3, 7)

#################################################################################
# Copyright (c) 2019, The Open Source Financial Developers Association
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose, without fee, and without a written agreement
# is hereby granted, provided that the above copyright notice and this
# paragraph and the following two paragraphs appear in all copies.
#
# IN NO EVENT SHALL The Open Source Financial Developers Association BE LIABLE
# TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
# DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
# AND ITS DOCUMENTATION, EVEN IF The Open Source Financial Developers Association
# HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The Open Source Financial Developers Association SPECIFICALLY DISCLAIMS
# ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
# THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
# AND The Open Source Financial Developers Association HAS NO OBLIGATIONS
# TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
#################################################################################

#
# TESTED:
#
#   DATE   	POSTGRES	OS
# 4/23/2019	11      	Ubuntu 18
#

import os
import sys
import errno

command_verb=os.path.splitext(os.path.basename(sys.argv[0]))[0]

# Internationalization...
from gettext import textdomain, gettext as _
textdomain (command_verb)

def err (message=''):
    sys.stderr.write (_(str(message))+'\n')

if sys.version_info<TESTED_PYTHON_VERSION:
   version=str(TESTED_PYTHON_VERSION[0])+'.'+str(TESTED_PYTHON_VERSION[1])
   err ('This script was only tested out for python '+version+';')
   err ('Please upgrade your python to run it '+
        '(or invoke it with "python'+str(TESTED_PYTHON_VERSION[0])+'"?)'
       )
   exit (errno.ENOPKG) # Package not iinstalled...

from collections import OrderedDict
import subprocess
import shutil
import zipfile
import re

def nix(): # Linux, OSx, or BSD -not Windows...
    return os.name!='nt' # nix

if nix():
   import tty, termios # used by getch...

else: # Windows...
      import msvcrt # used by getch...

ESUCCESS=0 # errno pseudo-return code...

DEFAULT_CERT_LIFE_YEARS=2
DEFAULT_CERT_LIFE_DAYS=DEFAULT_CERT_LIFE_YEARS*365

EXPIRY_DATE_TIME_FORMAT='%Y-%m-%d %-I:%M %p %Z' # yyyy-m-dd h:mm AM/PM ZONE
EXPIRY_UTC_DATE_TIME_FORMAT='%Y-%m-%d %H:%M UTC' # yyyy-m-dd H:mm UTC

# certificate files are SMALL, so you shouldn't need to play with this!
# (it's more about bundling the files and possibly encrypting them...)
ZIP_COMPRESSION_LEVEL=4 # 1-9, with 9 being the highest degree of compression...

POSTGRES_GROUP='postgres'
POSTGRES_USER='postgres'
MAX_POSTGRES_USERNAME_LENGTH=12 # used for --expiry...

# This script relies on these commands; they need to be on the shell path.
# If that should be difficult to configure, you can add a 'path' dictionary entry
# as demonstrated in the comments below...
PREREQUISITES=OrderedDict({'psql': {
#                                    'path': 'explicit_path_to_this_command',
                                    'sudo_user': POSTGRES_USER
                                   },
                           'openssl': {
#                                       'path': 'explicit_path_to_this_command'
                                      }
                          } # PREREQUISITES...
                         )
OPTIONS=['version', 'cert-life', 'expiry', 'zip', 'zip-encrypt', 'log']

if nix(): # premise: Bash...
   NO_SHELL_OUTPUT=' >/dev/null 2>/dev/null'

else: # Windows...
      NO_SHELL_OUTPUT=' >NUL:'

def version():
    return os.path.basename(__file__)+' Version '+VERSION # version

def before(text, subtext):
    offset=text.find(subtext)
    if offset<0:
       return None # before

    return text[:offset] # before

def after(text, subtext):
    offset=text.find(subtext)
    if offset<0:
       return None # after

    return text[offset+len(subtext):] # after

def unquote(value):
    value=value.strip()
    if len(value)>1 and \
       '\'"'.find(value[0])>=0 and \
       value[0]==value[-1]:
       value=value[1:-1].strip()

    return value # unquote

def plural(count):
    if count!=1:
       return 's' # plural

    return '' # plural

def rm(*paths):
    deleted=0
    for path in paths:
        if os.path.isfile(path):
           os.remove (path)
           deleted+=1

    return deleted # rm

def multi_arg(arg):
    return type(arg) is list or type(arg) is tuple # multi_arg

def chown (paths, user, group):
    if not multi_arg(paths):
       paths=[paths]

    for path in paths:
        shutil.chown (path, user, group)

def islength(text):
    if text is None:
       return False # islength

    if text.isdigit():
       return int(text)>0 # islength

    return False # islength

def days(life): # None =--> not a valid life...
    if life is None:
       return None # days

    life=unquote(life).strip().split()
    if len(life)!=2 or \
       not life[0].isdigit():
       return None # days

    n=int(life[0])
    if n==0:
       return None # days

    if life[1]=='days' or \
       n==1 and life[1]=='day':
       return n # days

    if life[1]=='weeks' or \
       n==1 and life[1]=='week':
       return 7*n # days

    if life[1]=='months' or \
       n==1 and life[1]=='month':
       return 30*n # based on INTEGER average...

    if life[1]=='years' or \
       n==1 and life[1]=='year':
       return 365*n # days

    return None # days

def life(days):
    def span(days, period, name):
        n=days//period
        if n>0 and days%period==0:
           return str(n)+' '+name # span

        return None # span

    term=span(days, 365, 'years')
    if not (term is None):
       return term # life

    term=span(days, 30, 'months') # based on INTEGER average...
    if not (term is None):
       return term # life

    term=span(days, 7, 'weeks')
    if not (term is None):
       return term # life

    return str(days)+' days' # life

def log_comments (log_path, comments='', echo=False):
    if echo:
       err (comments)

    if nix():
       comment_char='#' # premise: Bash...

    else: # Windows...
          comment_char='::' # premise: Windows NT cmd...

    with open(log_path, 'a') as log_file:
         for comment in comments.splitlines():
             log_file.write (comment_char+' '+comment+'\n')

def shell_output(command):
    if '\n' in command:
       if nix(): # premise: bash...
          command=command.replace('\n', ' \\\n')

       else: # Windows...
             command='('+command.replace('\n', '\n ')+'\n)'

    # Note that shell=true appears to be "safer" on Macs...
    return subprocess.check_output(command, shell=True
                                  ).decode('utf-8').strip() # shell_output

def shell_exit(command, echo=True, log=None):
    if (not (log is None)) and \
       ('>' in log or \
        '|' in log \
       ):
       # If you use the "log" argument, DON'T do redirection in "command"
       # (since we use redirection to accomplish writing to "log"...)
       err ('Configuration error: redirection use in "shell_exit" with "log" argument;')
       err ('contact the developer of this package to correct it.')
       err ('Exiting...')
       exit (errno.EINVAL) # Invalid argument...

    if '\n' in command:
       if nix(): # premise: bash...
          command=command.replace('\n', ' \\\n')

       else: # Windows...
             command='('+command.replace('\n', '\n ')+'\n)'

    if echo:
       print (command)
       sys.stdout.flush ()
       if not (log is None):
          with open(log, 'a') as log_file:
               log_file.write (command+'\n')

    # Note that shell=True appears to be "safer" on Macs...
    if not (log is None):
       command+=' >"'+log+'.new"'
       if nix():
          command+=' 2>"'+log+'.new"' # stderr too...

    status=subprocess.call(command, shell=True)
    if not (log is None):
       with open(log+'.new') as log_file:
            log_out=log_file.read()

       sys.stderr.write (log_out)

       if nix():
          comment_char='#' # premise: Bash...

       else: # Windows...
             comment_char='::' # premise: Windows NT cmd...

       with open(log, 'a') as log_file:
            for line in log_out.splitlines():
                log_file.write (comment_char+' '+line+'\n')

            log_file.write ('\n') # space the commands out...

       rm (log+'.new')

    return status # shell_exit

def required(prerequisite):
    if 'platform' in PREREQUISITES[prerequisite]:
       if PREREQUISITES[prerequisite]['platform']=='nix' and nix() or \
          PREREQUISITES[prerequisite]['platform']=='osx' and osx():
          return True # required

       elif PREREQUISITES[prerequisite]['platform']=='windows' and not nix():
            return True # required

       return False # required

    return True # required

def prerequisites():
    reqs=[]
    for prerequisite in PREREQUISITES:
        if required(prerequisite) and \
           not (prerequisite in reqs):
           reqs+=[prerequisite]

    return ', '.join(reqs) # prerequisites

def prerequisite_command(prerequisite):
    if 'path' in PREREQUISITES[prerequisite]:
       return PREREQUISITES[prerequisite]['path'] # prerequisite_command

    return prerequisite # prerequisite_command

def check_prerequisites():
    missing={}
    for prerequisite in PREREQUISITES:
        if required(prerequisite) and \
           shutil.which(prerequisite_command(prerequisite)) is None:
           missing[prerequisite]=True

    if len(missing)>0:
       err ('This script relies on the following command'+plural(len(missing))+':')
       for prerequisite in missing:
           if missing is None:
              qualifier=''

           else: # not (missing is None)
                 qualifier=' (run as '+missing[prerequisite]+')'

           err ('\t'+prerequisite+qualifier)

       if len(missing)==1:
          preposition='it is'

       else: # len(missing)!=1
             preposition='they are'

       err ()
       err ('make sure '+preposition+' on your command path.')
       err ('Exiting...')
       exit (errno.ENOPKG) # Package not installed...

def getch(prompt=None, default=None):
    if not (prompt is None):
       sys.stdout.write (prompt)

    sys.stdout.flush ()
    if nix():
       stdin=sys.stdin.fileno()
       settings=termios.tcgetattr(stdin)
       try:
            tty.setraw (stdin)
            ch=sys.stdin.read(1)

       finally:
                termios.tcsetattr (stdin,
                                   termios.TCSADRAIN,
                                   settings
                                  )
    else: # Windows...
          ch=msvcrt.getch().decode()

    if ch!='\r':
       print (ch)

    else: # ch=='\r'
          print ()

    if not (default is None):
       if ch=='\r':
          ch=default

    return ch # getch

def confirm(prompt, default=None):
    answer=getch(prompt,
                 default=default
                ).lower()
    while not (answer in 'yn'):
          answer=getch(prompt,
                       default=default
                      ).lower()

    return answer=='y' # confirm

def sudo(command, user=''):
    if user!='':
       context='--user '+user+' --login '

    return shell_output('sudo '+context+command) # sudo

def grep(pattern, path):
    compiled_pattern=re.compile(pattern)
    with open(path, 'r') as file:
         line=file.readline()
         while line:
               if not (compiled_pattern.search(line) is None):
                  return True # grep

               line=file.readline()

    return False # grep

def pip_install(module):
    try:
         subprocess.check_call ([sys.executable, '-m', 'pip', 'install', module])

    except Exception as exception:
           err (exception)
           return False # pip_install

    return True # pip_install

def key_length(cert_path):
    summary=shell_output(prerequisite_command('openssl')+' x509 -noout '+
                                                               '-text '+
                                                               '-in "'+cert_path+'"'
                        )
    for line in summary.splitlines():
        if 'Public-Key:' in line:
           public_key=after(line, 'Public-Key:').strip()
           if '(' in public_key:
              public_key=before(after(public_key, '('), ')').strip()

           public_key=public_key.split(' ')
           length=public_key[0]
           if length.isdigit():
              return int(length) # key_length

           break # for line in summary.splitlines()

    return None # key_length

def common_name(cert_path):
    subject=shell_output(prerequisite_command('openssl')+' x509 -noout '+
                                                               '-subject '+
                                                               '-in "'+cert_path+'"'
                        )
    if subject.startswith('subject='):
       subject=after(subject, 'subject=')

    subject=subject.replace(' = ', '=').split(',')
    cn=None
    for subject_part in subject:
        subject_part=subject_part.strip()
        if subject_part.startswith('CN='):
           cn=after(subject_part, 'CN=').strip()
           break # for subject_part in subject
       
    return cn # common_name

def psql_show(item):
    command=prerequisite_command('psql')+" --tuples-only --command 'SHOW "+item+"'"
    if nix():
       return sudo(command, POSTGRES_USER) # psql_show

    return shell_output(command) # psql_show

def expiry(cert_path, timezone):
    try:
         from dateutil.parser import parse

    except:
            err ('Adding required dateutil module to python...')
            if not pip_install('python-dateutil'):
               exit (errno.ENOPKG) # Package not installed...
                
            try:
                 from dateutil.parser import parse

            except Exception as exception:
                   err (exception)
                   exit (errno.ENOPKG) # Package not installed...

    if not (timezone is None):
       from dateutil import tz

    summary=shell_output(prerequisite_command('openssl')+' x509 -noout '+
                                                               '-text '+
                                                               '-in "'+cert_path+'"'
                        ).splitlines()
    for line in summary:
        if 'Not After' in line:
           if ':' in line:
              expiration=parse(after(line, ':'))
              if not (timezone is None):
                 try:
                      local_timezone=tz.gettz(timezone)

                 except:
                         local_timezone=None

                 if local_timezone is None:
                    err ('Timezone must be in "tzdata" format '+
                         '(example: "America/New_York"...)'
                        )
                    exit (errno.EINVAL) # Invalid argument...

                 expiration=expiration.astimezone(local_timezone)
                 return expiration.strftime(EXPIRY_DATE_TIME_FORMAT) # expiry

              return expiration.strftime(EXPIRY_UTC_DATE_TIME_FORMAT) # expiry

           break # for line in summary

    return None # expiry

users=[]
options={}
for arg in sys.argv[1:]:
    if arg.startswith('--'):
       arg=after(arg, '--')
       if '=' in arg:
          value=unquote(after(arg, '='))
          option=before(arg, '=')

       else: # not ('=' in arg)
             option=arg
             value=None

       options[option]=value

    else: # not arg.startswith('--')
          users+=arg.split(',')

unknown_option=False
for option in options:
    unknown_option=not (option in OPTIONS)
    if unknown_option:
       break # for option in options

config_path=psql_show('config_file')
if config_path=='':
   ssl_dir=None

else: # config_path!=''
      config_dir=os.path.dirname(config_path)
      ssl_dir=config_dir+'/ssl'
      server_crt=ssl_dir+'/server.crt'

if 'cert-life' in options:
   cert_life_days=days(options['cert-life'])

else: # not ('cert-life' in options)
      cert_life_days=DEFAULT_CERT_LIFE_DAYS

if len(users)==0 and \
   len(options)==1 and \
   'version' in options:
   print (version())
   exit (ESUCCESS)

if len(users)==0 and \
   len(options)==1 and \
   'expiry' in options:
   if ssl_dir is None:
      err ('Error retrieving postgres config file; exiting...')
      exit (errno.ENOENT) # No such file or directory...
      
   if not os.path.isfile(server_crt):
      err ('Server certificate not generated yet')
      err ('(you can use "'+COMPANION_SCRIPT+'" to do that...)')
      exit (errno.ENOENT) # No such file or directory...

   check_prerequisites ()
   for user in os.listdir(ssl_dir):
       user_dir=ssl_dir+'/'+user
       if os.path.isdir(user_dir) and \
          (not user in ['.', '..']):
          hostname=common_name(server_crt)
          client_crt=user_dir+'/'+hostname+'.crt'
          if os.path.isfile(client_crt):
             print (user.ljust(MAX_POSTGRES_USERNAME_LENGTH)+': '+
                    expiry(client_crt, options['expiry'])
                   )
             
   exit (ESUCCESS)

if len(users)==0 or unknown_option or \
   cert_life_days is None:
   err ('Usage:')
   err ()
   err ('\t'+command_verb+' '+
             '{--version | [user_list] {--version}\n'+
        '\t\t{--cert-life="[n] <days|weeks|months|years>"} {--zip|--zip-encrypt} {--log=[path]} | \n'+
        '\t\t--expiry{="[time_zone]"}'
       )
   err ()
   err ('where "user_list" is a list of one or more POSTGRES users.')
   if ssl_dir is None:
      ssl_dir='.../ssl'

   err ()
   err ('Generates a client certificate and key for one or more users in:')
   err ()
   err ('\t'+ssl_dir+'/[postgres_user]')
   err ()
   err ('The CLIENT certificates and keys can be shared via a secure channel')
   err ('(such as Signal or offline media...) A zip (or password-protected zip)')
   err ('can also be generated of the keys for each user, zips are generated in:')
   err ()
   err ('\t'+ssl_dir)
   err ()
   err ('accordingly named: [postgres_user].zip')
   err ()
   err ('If "--cert-life" is omitted, '+
        'a default of '+life(DEFAULT_CERT_LIFE_DAYS)+' will be used.'
       )
   err ()
   err ('A log is generated by default in:\n'+
        '\t'+ssl_dir+'/[postgres_user].log'
       )
   err ()
   err ('Prerequisite external commands: '+prerequisites())
   exit (errno.EINVAL) # Invalid argument...

check_prerequisites()

if config_path=='':
   err ('Error retrieving postgres config file; exiting...')
   exit (errno.ENOENT) # No such file or directory...

server_key=ssl_dir+'/server.key'
ca_crt=ssl_dir+'/ca.crt' # a.k.a., the "root"...

if not os.path.isfile(ca_crt):
   err ('Missing "'+ca_crt+'"; run "'+COMPANION_SCRIPT+'" first to generate that...')
   exit (errno.ENOENT)

if not os.path.isfile(server_key):
   err ('Missing "'+server_key+'"; run "'+COMPANION_SCRIPT+'" first to generate that...')
   exit (errno.ENOENT)

hostname=common_name(server_crt)
if hostname is None:
   err ('Cannot determine the server hostname (i.e., the common name...)')
   err ('\tfrom: '+server_crt)
   err ('Exiting...')
   exit (errno.ENODATA) # No data available...

if 'zip' in options or 'zip-encrypt' in options:
   if 'zip-encrypt' in options:
      import getpass

   try:
        import pyminizip

   except:
           err ('Adding required zip module to python...')
           if not pip_install('pyminizip'):
              exit (errno.ENOPKG) # Package not installed...
                
           try:
                import pyminizip

           except Exception as exception:
                  err (exception)
                  exit (errno.ENOPKG) # Package not installed...

global_log='log' in options and \
           not (options['log'] is None)

if 'version' in options:
   err (version())

if global_log:
   log_path=options['log']
   if 'version' in options:
      log_comments (log_path, version())

certs=0
for user in users:
    user_dir=ssl_dir+'/'+user
    if not os.path.isdir(user_dir):
       os.mkdir (user_dir)

    if nix():
       chown (user_dir, POSTGRES_USER, POSTGRES_GROUP)

    path_prefix=user_dir+'/'+hostname+'.'
    client_key=path_prefix+'key'
    client_crt=path_prefix+'crt'
    user_ca_crt=path_prefix+'ca'
    client_csr=path_prefix+'csr'

    if os.path.isfile(client_key) or \
       os.path.isfile(client_crt):
       err ('A client key was already generated for postgres user "'+user+'";')
       err ('generating another might require that client to download the new keys.')
       proceed=confirm('Proceed? [yN] ', default='n')

    else:
          proceed=True

    if proceed:
       certs+=1
       rm (client_key, client_crt, client_csr)
       if not global_log:
          if user=='openssl':
             log_path=ssl_dir+'/openssl_user.log' # prevent name collision (not likely...)

          else: # user!='openssl'
                log_path=ssl_dir+'/'+user+'.log'

          rm (log_path)
          if 'version' in options:
             log_comments (log_path, version())

       status=shell_exit(prerequisite_command('openssl')+' genrsa -out "'+client_key+'"\n'+
                                '               '+str(key_length(server_crt)),
                         log=log_path
                        )
       if status!=ESUCCESS:
          rm (client_key, client_crt, client_csr)
          exit (status)

       status=shell_exit(prerequisite_command('openssl')+' req -new\n'+
                         '            -key "'+client_key+'"\n'+
                         '            -subj "/CN='+user+'"\n'+
                         '            -out "'+client_csr+'"',
                         log=log_path
                        )
       if status!=ESUCCESS:
          rm (client_key, client_crt, client_csr)
          exit (status)

       status=shell_exit(prerequisite_command('openssl')+' x509 -req\n'+
                         '             -in "'+client_csr+'"\n'+
                         '             -days '+str(cert_life_days)+'\n'+
                         '             -CA "'+ca_crt+'"\n'+
                         '             -CAkey "'+server_key+'"\n'+
                         '             -CAcreateserial\n'+
                         '             -out "'+client_crt+'"',
                         log=log_path
                        )
       if status!=ESUCCESS:
          rm (client_key, client_crt, client_csr)
          exit (status)

       log_comments (log_path, life(cert_life_days))

       rm (client_csr)

       if os.path.isfile(user_ca_crt):
          rm (user_ca_crt)

       os.symlink (ca_crt, user_ca_crt)
       log_comments (log_path,
                     'link made from "'+ca_crt+'"\n'+
                     '\tto "'+user_ca_crt+'"'
                    )

       err ('Generated:')
       err ('\t'+user_ca_crt)
       err ('\t'+client_key)
       err ('\t'+client_crt)
       if 'zip' in options or 'zip-encrypt' in options:
          zip=ssl_dir+'/'+user+'.zip'
          if 'zip-encrypt' in options:
             pw=''
             reenter_pw=None
             while pw!=reenter_pw:
                   while pw=='':
                         # you could conceivably get stuck in a prompt loop here
                         # if you change your mind and don't want to enter one;
                         # an expereinced admin will know to do a Ctrl-C and
                         # start over...
                         pw=getpass.getpass('Password for '+user+'.zip> ')

                   reenter_pw=''
                   while reenter_pw=='':
                         reenter_pw=getpass.getpass('Reenter Password for '+user+'.zip> ')
          else: # not ('zip-encrypt' in options)
                pw=None

          if os.path.isfile(zip):
             rm (zip)

          pyminizip.compress_multiple ([user_ca_crt, client_key, client_crt],
                                       [], # default paths of eachofthe preceding list...
                                       zip,
                                       pw,
                                       ZIP_COMPRESSION_LEVEL
                                      )
          err ('\t'+zip)

       err ()

if certs>0:
   users=','.join(users)
   password_encryption=psql_show('password_encryption')
   hba_path=psql_show('hba_file')
   if password_encryption=='scram-sha-256':
      if not grep('^hostssl.*\s'+users+'\s.*scram-sha-256', hba_path):
         err ('Be sure to configure a "hostssl...scram-sha-256" entry in "'+hba_path+'",')
         err ('and then restart the postgres server.')
         err ()
         err ('For example, to allow access to all databases from all addresses')
         err ('for the user'+plural(len(users))+' you specified, the entry would be:')
         err ()
         err ('# TYPE...DATABASE...USER(S)...ADDRESS...METHOD...OPTIONS')
         err ('hostssl\tall\t'+users+'\t0.0.0.0/0\tscram-sha-256')
         err ()

   elif password_encryption=='md5':
        if not grep('^hostssl.*\s'+users+'\s.*clientcert=1', hba_path):
           err ('Be sure to configure a "hostssl...clientcert" entry in "'+hba_path+'",')
           err ('and then restart the postgres server.')
           err ()
           err ('For example, to allow access to all databases from all addresses')
           err ('for the user'+plural(len(users))+' you specified, the entry would be:')
           err ()
           err ('# TYPE...DATABASE...USER(S)...ADDRESS...METHOD...OPTIONS')
           err ('hostssl\tall\t'+users+'\t0.0.0.0/0\tmd5\tclientcert=1')
           err ()
