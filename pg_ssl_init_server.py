#!/usr/bin/python3
#
# pg_ssl_init_server.py
#
# pg_ssl_init_server [host_name]
#
# generates server keys for SSL access (typically for pgadmin...)
# in an "ssl" subfolder of the postgres configuration files
#
# prerequisite external commands: psql, openssl
# (+systemctl on Linux, +pg_ctl on OSx...)
#

VERSION='12.2.12' # based on testing with postgres 12.2...
COMPANION_SCRIPT='pg_ssl_init_client'
TESTED_PYTHON_VERSION=(3, 7)

#################################################################################
# Copyright (c) 2019, The Open Source Financial Developers Association
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose, without fee, and without a written agreement
# is hereby granted, provided that the above copyright notice and this
# paragraph and the following two paragraphs appear in all copies.
#
# IN NO EVENT SHALL The Open Source Financial Developers Association BE LIABLE
# TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
# DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
# AND ITS DOCUMENTATION, EVEN IF The Open Source Financial Developers Association
# HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The Open Source Financial Developers Association SPECIFICALLY DISCLAIMS
# ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
# THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
# AND The Open Source Financial Developers Association HAS NO OBLIGATIONS
# TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
#################################################################################

#
# TESTED:
#
#   DATE   	POSTGRES	OS
# 4/23/2019	11      	Ubuntu 18
#

import os
import sys
import errno

command_verb=os.path.splitext(os.path.basename(sys.argv[0]))[0]

# Internationalization...
from gettext import textdomain, gettext as _
textdomain (command_verb)

def err (message=''):
    sys.stderr.write (_(message)+'\n')

if sys.version_info<TESTED_PYTHON_VERSION:
   version=str(TESTED_PYTHON_VERSION[0])+'.'+str(TESTED_PYTHON_VERSION[1])
   err ('This script was only tested out for python '+version+';')
   err ('Please upgrade your python to run it '+
        '(or invoke it with "python'+str(TESTED_PYTHON_VERSION[0])+'"?)'
       )
   exit (errno.ENOPKG) # Package not iinstalled...

from collections import OrderedDict
import subprocess
import shutil
from datetime import datetime, timezone
import time

def nix(): # Linux, OSx, or BSD -not Windows...
    return os.name!='nt' # nix

def osx():
    return sys.platform=='darwin' # osx

if nix():
   import tty, termios # used by getch...

else: # Windows...
      import msvcrt # used by getch...

ESUCCESS=0 # errno pseudo-return code...

DEFAULT_KEY_LENGTH=2048

DEFAULT_CERT_LIFE_YEARS=2
DEFAULT_CERT_LIFE_DAYS=DEFAULT_CERT_LIFE_YEARS*365

EXPIRY_DATE_TIME_FORMAT='%Y-%m-%d %-I:%M %p %Z' # yyyy-m-dd h:mm AM/PM ZONE
EXPIRY_UTC_DATE_TIME_FORMAT='%Y-%m-%d %H:%M UTC' # yyyy-m-dd H:mm UTC

BACKUP_TIMESTAMP_FORMAT='%Y-%m-%d_%H-%M-%S'

POSTGRES_GROUP='postgres'
POSTGRES_USER='postgres'

# This script relies on these commands; they need to be on the shell path.
# If that should be difficult to configure, you can add a 'path' dictionary entry
# as demonstrated in the comments below...
PREREQUISITES=OrderedDict({'psql': {
#                                    'path': 'explicit_path_to_this_command',
                                    'sudo_user': POSTGRES_USER
                                   },
                           'openssl': {
#                                       'path': 'explicit_path_to_this_command'
                                      },
                           'ping': {
#                                    'path': 'explicit_path_to_this_command'
                                   },
                           'systemctl': {
#                                         'path': 'explicit_path_to_this_command'
                                         'platform': 'nix'
                                        },
                           'pg_ctl': {
#                                      'path': 'explicit_path_to_this_command',
                                      'platform': 'osx'
                                     }
                          }
                         ) # PREREQUISITES...
OPTIONS=['version', 'key-length', 'cert-life', 'expiry', 'log']
DEFAULT_LOG='openssl.log'

RESTART_SECONDS=5
PGDATA='' # can be used for pg_ctl on OSx, IF it is not defined in the environment...

if nix(): # premise: Bash...
   NO_SHELL_OUTPUT=' >/dev/null 2>/dev/null'

else: # Windows...
      NO_SHELL_OUTPUT=' >NUL:'

def version():
    return os.path.basename(__file__)+' Version '+VERSION # version

def before(text, subtext):
    offset=text.find(subtext)
    if offset<0:
       return None # before

    return text[:offset] # before

def after(text, subtext):
    offset=text.find(subtext)
    if offset<0:
       return None # after

    return text[offset+len(subtext):] # after

def unquote(value):
    value=value.strip()
    if len(value)>1 and \
       '\'"'.find(value[0])>=0 and \
       value[0]==value[-1]:
       value=value[1:-1].strip()

    return value # unquote

def plural(count):
    if count==1:
       return '' # plural

    return 's' # plural

def rm(*paths):
    deleted=0
    for path in paths:
        if os.path.isfile(path):
           os.remove (path)
           deleted+=1

    return deleted # rm

def multi_arg(arg):
    return type(arg) is list or type(arg) is tuple # multi_arg

def chown (paths, user, group):
    if not multi_arg(paths):
       paths=[paths]

    for path in paths:
        shutil.chown (path, user, group)

def islength(text):
    if text is None:
       return False # islength

    if text.isdigit():
       return int(text)>0 # islength

    return False # islength

def days(life): # None =--> not a valid life...
    if life is None:
       return None # days

    life=unquote(life).strip().split()
    if len(life)!=2 or \
       not life[0].isdigit():
       return None # days

    n=int(life[0])
    if n==0:
       return None # days

    if life[1]=='days' or \
       n==1 and life[1]=='day':
       return n # days

    if life[1]=='weeks' or \
       n==1 and life[1]=='week':
       return 7*n # days

    if life[1]=='months' or \
       n==1 and life[1]=='month':
       return 30*n # based on INTEGER average...

    if life[1]=='years' or \
       n==1 and life[1]=='year':
       return 365*n # days

    return None # days

def life(days):
    def span(days, period, name):
        n=days//period
        if n>0 and days%period==0:
           return str(n)+' '+name

        return None # span

    term=span(days, 365, 'years')
    if not (term is None):
       return term # life

    term=span(days, 30, 'months') # based on INTEGER average...
    if not (term is None):
       return term # life

    term=span(days, 7, 'weeks')
    if not (term is None):
       return term # life

    return str(days)+' days' # life

def log_comments (log_path, comments='', echo=False):
    if echo:
       err (comments)

    if nix():
       comment_char='#' # premise: Bash...

    else: # Windows...
          comment_char='::' # premise: Windows NT cmd...

    with open(log_path, 'a') as log_file:
         for comment in comments.splitlines():
             log_file.write (comment_char+' '+comment+'\n')

def shell_output(command):
    if '\n' in command:
       if nix(): # premise: bash...
          command=command.replace('\n', ' \\\n')

       else: # Windows...
             command='('+command.replace('\n', '\n ')+'\n)'

    # Note that shell=true appears to be "safer" on Macs...
    return subprocess.check_output(command, shell=True
                                  ).decode('utf-8').strip() # shell_output

def shell_exit(command, echo=True, log=None):
    if (not (log is None)) and \
       ('>' in log or \
        '|' in log \
       ):
       # If you use the "log" argument, DON'T do redirection in "command"
       # (since we use redirection to accomplish writing to "log"...)
       err ('Configuration error: redirection use in "shell_exit" with "log" argument;')
       err ('contact the developer of this package to correct it.')
       err ('Exiting...')
       exit (errno.EINVAL) # Invalid argument...

    if '\n' in command:
       if nix(): # premise: bash...
          command=command.replace('\n', ' \\\n')

       else: # Windows...
             command='('+command.replace('\n', '\n ')+'\n)'

    if echo:
       print (command)
       sys.stdout.flush ()
       if not (log is None):
          with open(log, 'a') as log_file:
               log_file.write (command+'\n')

    # Note that shell=True appears to be "safer" on Macs...
    if not (log is None):
       command+=' >"'+log+'.new"'
       if nix():
          command+=' 2>"'+log+'.new"' # stderr too...

    status=subprocess.call(command, shell=True)
    if not (log is None):
       with open(log+'.new') as log_file:
            log_out=log_file.read()

       sys.stderr.write (log_out)

       if nix():
          comment_char='#' # premise: Bash...

       else: # Windows...
             comment_char='::' # premise: Windows NT cmd...

       with open(log, 'a') as log_file:
            for line in log_out.splitlines():
                log_file.write (comment_char+' '+line+'\n')

            log_file.write ('\n') # space the commands out...

       rm (log+'.new')

    return status # shell_exit

def required(prerequisite):
    if 'platform' in PREREQUISITES[prerequisite]:
       if PREREQUISITES[prerequisite]['platform']=='nix' and nix() or \
          PREREQUISITES[prerequisite]['platform']=='osx' and osx():
          return True # required

       elif PREREQUISITES[prerequisite]['platform']=='windows' and not nix():
            return True # required

       return False # required

    return True # required

def prerequisites():
    reqs=[]
    for prerequisite in PREREQUISITES:
        if required(prerequisite) and \
           not (prerequisite in reqs):
           reqs+=[prerequisite]

    return ', '.join(reqs) # prerequisites

def prerequisite_command(prerequisite):
    if 'path' in PREREQUISITES[prerequisite]:
       return PREREQUISITES[prerequisite]['path'] # prerequisite_command

    return prerequisite # prerequisite_command

def check_prerequisites():
    missing={}
    for prerequisite in PREREQUISITES:
        if required(prerequisite) and \
           shutil.which(prerequisite_command(prerequisite)) is None:
           missing[prerequisite]=True

    if len(missing)>0:
       err ('This script relies on the following command'+plural(len(missing))+':')
       for prerequisite in missing:
           if missing is None:
              qualifier=''

           else: # not (missing is None)
                 qualifier=' (run as '+missing[prerequisite]+')'

           err ('\t'+prerequisite+qualifier)

       if len(missing)==1:
          preposition='it is'

       else: # len(missing)!=1
             preposition='they are'

       err ()
       err ('make sure '+preposition+' on your command path.')
       err ('Exiting...')
       exit (errno.ENOPKG) # Package not installed...

def getch(prompt=None, default=None):
    if not (prompt is None):
       sys.stdout.write (prompt)

    sys.stdout.flush ()
    if nix():
       stdin=sys.stdin.fileno()
       settings=termios.tcgetattr(stdin)
       try:
            tty.setraw (stdin)
            ch=sys.stdin.read(1)

       finally:
                termios.tcsetattr (stdin,
                                   termios.TCSADRAIN,
                                   settings
                                  )
    else: # Windows...
          ch=msvcrt.getch().decode()

    if ch!='\r':
       print (ch)

    else: # ch=='\r'
          print ()

    if not (default is None):
       if ch=='\r':
          ch=default

    return ch # getch

def confirm(prompt, default=None):
    answer=getch(prompt,
                 default=default
                ).lower()
    while not (answer in 'yn'):
          answer=getch(prompt,
                       default=default
                      ).lower()

    return answer=='y' # confirm

def sudo(command, user=''):
    if user!='':
       context='--user '+user+' --login '

    return shell_output('sudo '+context+command) # sudo

def pip_install(module):
    try:
         subprocess.check_call ([sys.executable, '-m', 'pip', 'install', module])

    except Exception as exception:
           err (exception)
           return False # pip_install

    return True # pip_install

def psql_show(item):
    command=prerequisite_command('psql')+" --tuples-only --command 'SHOW "+item+"'"
    if nix():
       return sudo(command, POSTGRES_USER) # psql_show

    return shell_output(command) # psql_show

def expiry(cert_path, timezone):
    try:
         from dateutil.parser import parse

    except:
            err ('Adding required dateutil module to python...')
            if not pip_install('python-dateutil'):
               exit (errno.ENOPKG) # Package not installed...
                
            try:
                 from dateutil.parser import parse

            except Exception as exception:
                   err (exception)
                   exit (errno.ENOPKG) # Package not installed...

    if not (timezone is None):
       from dateutil import tz

    summary=shell_output(prerequisite_command('openssl')+' x509 -noout '+
                                                               '-text '+
                                                               '-in "'+cert_path+'"'
                        ).splitlines()
    for line in summary:
        if 'Not After' in line:
           if ':' in line:
              expiration=parse(after(line, ':'))
              if not (timezone is None):
                 try:
                      local_timezone=tz.gettz(timezone)

                 except:
                         local_timezone=None

                 if local_timezone is None:
                    err ('Timezone must be in "tzdata" format '+
                         '(example: "America/New_York"...)'
                        )
                    exit (errno.EINVAL) # Invalid argument...

                 expiration=expiration.astimezone(local_timezone)
                 return expiration.strftime(EXPIRY_DATE_TIME_FORMAT) # expiry

              return expiration.strftime(EXPIRY_UTC_DATE_TIME_FORMAT) # expiry

           break # for line in summary

    return None # expiry

def utc_timestamp():
    return datetime.now(timezone.utc).strftime(BACKUP_TIMESTAMP_FORMAT)+'_UTC' # utc_timestamp

def decomment(line, comment_char='#'):
    line=line.strip()
    quote=None
    offset=0
    while offset<len(line):
          if line[offset]==comment_char:
             if quote is None:
                return line[:offset].strip() # decomment

          elif not (quote is None):
               if line[offset]==quote:
                  quote=None

               elif line[offset]=='\\':
                    line=line[:offset]+line[offset+1]

          elif line[offset]=="'" or \
               line[offset]=='"':
               quote=line[offset]

          offset+=1

    return line # decomment

def crunch(text):
    return text.replace(' ', '').replace('\t', '') # crunch

def config_get(config, key):
    for index in range(len(config)):
        line=decomment(config[index])
        if crunch(line).startswith(key+'='):
           return unquote(after(line, '=')) # config_get

    return None # config_get

def config_set (config, key, value):
    for index in range(len(config)):
        line=decomment(config[index])
        if crunch(line).startswith(key+'='):
           config[index]=key+"='"+value+"'"
           return # config_set

    config+=[key+"='"+value+"'"] # append it...

config_path=psql_show('config_file')
if config_path=='':
   ssl_dir=None

else: # config_path!=''
      config_dir=os.path.dirname(config_path)
      ssl_dir=config_dir+'/ssl'
      server_crt=ssl_dir+'/server.crt'

args=[]
options={}
for arg in sys.argv[1:]:
    if arg.startswith('--'):
       arg=after(arg, '--')
       if '=' in arg:
          value=unquote(after(arg, '='))
          option=before(arg, '=')

       else: # not ('=' in arg)
             option=arg
             value=None

       options[option]=value

    else: # not arg.startswith('--')
          args+=arg.split(',')

unknown_option=False
for option in options:
    unknown_option=not (option in OPTIONS)
    if unknown_option:
       break # for option in options

if len(args)==0 and \
   len(options)==1 and \
   'version' in options:
   print (version())
   exit (ESUCCESS)

if len(args)==0 and \
   len(options)==1 and \
   'expiry' in options:
   if ssl_dir is None:
      err ('Error retrieving postgres config file; exiting...')
      exit (errno.ENOENT) # No such file or directory...

   if not os.path.isfile(server_crt):
      err ('Server certificate not generated yet...')
      exit (errno.ENOENT) # No such file or directory...

   check_prerequisites ()
   print (expiry(server_crt, options['expiry']))
   exit (ESUCCESS)

if 'key-length' in options:
   key_length=options['key-length']

else: # not ('key-length' in options)
      key_length=str(DEFAULT_KEY_LENGTH)

if 'cert-life' in options:
   cert_life_days=days(options['cert-life'])

else: # not ('cert-life' in options)
      cert_life_days=DEFAULT_CERT_LIFE_DAYS

if len(args)!=1 or unknown_option or \
   (not islength(key_length)) or \
   cert_life_days is None:
   err ('Usage:')
   err ()
   err ('\t'+command_verb+' '+
            '{[host_name] {--version} {--key-length=[key_length]}\n'+
        '\t\t{--cert-life="[n] <days|weeks|months|years>"} {--log=[path]} | \n'+
        '\t\t--expiry{="[time_zone]"}'
       )
   err ("""
where "host_name" is the DNS or locally defined name (or IP address...)
that will be used by a client to connect to the server.

Generates a server certificate and key for SSL access (typically for pgadmin...)
in an "ssl" subfolder of the postgres configuration files."""
       )
   if ssl_dir is None:
      log_path='.../ssl'

   else: # we got an ssl_dir; add an example path to this Usage...
         err ('[In this instance: '+ssl_dir+']')
         log_path=ssl_dir

   err ()
   err ('If "--key-length" is omitted, '+
        'a default of '+str(DEFAULT_KEY_LENGTH)+' will be used.'
       )
   err ('If "--cert-life" is omitted, '+
        'a default of '+life(DEFAULT_CERT_LIFE_DAYS)+' will be used.'
       )
   err ()
   err ("""After this utility is initially run, individual client certificates and keys
can be generated by using the companion utiity "pg_generate_client_key"."""
       )

   log_path+='/'+DEFAULT_LOG
   err ()
   err ('A log is generated by default in:\n'+
        '\t'+log_path
       )
   err ()
   err ('Prerequisite external commands: '+prerequisites())
   exit (errno.EINVAL) # Invalid argument...

check_prerequisites ()

hostname=args[0]

if config_path=='':
   err ('Error retrieving postgres config file; exiting...')
   exit (errno.ENOENT) # No such file or directory...

if not os.path.isdir(ssl_dir):
   os.mkdir (ssl_dir)

if nix():
   chown (ssl_dir, POSTGRES_USER, POSTGRES_GROUP)

server_key=ssl_dir+'/server.key'
ca_crt=ssl_dir+'/ca.crt'

if shell_exit('ping -c 1 '+hostname+' 1>/dev/null 2>/dev/null',
              echo=False
             )!=ESUCCESS:
   err ('Warning: host name "'+hostname+'" is not responding to a ping')
   err ('(sometimes that can be OK, if the name is only defined')
   err (' for external access in DNS or a client\'s "hosts" file...);')
   if not confirm('Proceed? [Yn] ', default='y'):
      exit (errno.EHOSTUNREACH) # No route to host...

if os.path.isfile(server_key) or os.path.isfile(server_crt):
   err ('A server key was already generated for postgres;')
   err ('generating another might require clients to download new keys.')
   if not confirm('Proceed? [yN] ', default='n'):
      exit (errno.EEXIST) # File exists...

key_length=int(key_length)

if 'log' in options and \
   not (options['log'] is None):
   log_path=options['log']

else:
      log_path=ssl_dir+'/'+DEFAULT_LOG
      rm (log_path)

if 'version' in options:
   log_comments (log_path, version(), echo=True)

rm (server_key, server_crt, ca_crt)

status=shell_exit(prerequisite_command('openssl')+' req -nodes -new -x509\n'+
                  '            -newkey rsa:'+str(key_length)+'\n'+
                  '            -days '+str(cert_life_days)+'\n'+
                  '            -subj "/CN='+hostname+'"\n'+
                  '            -keyout "'+server_key+'"\n'+
                  '            -out "'+server_crt+'"',
                  log=log_path
                 )
if status!=ESUCCESS:
   exit (status)

log_comments (log_path, life(cert_life_days))

shutil.copyfile (server_crt, ca_crt)

if nix():
   command='chmod u=r,g=,o= '+server_key
   if shell_exit(command, echo=False)!=ESUCCESS:
      err ('command failed: '+command)
      err ('exiting...')
      exit (errno.EACCES) # Permission denied...

   chown ([server_key, server_crt, ca_crt],
          POSTGRES_USER, POSTGRES_GROUP
         )

err ('Generated:')
err ('\t'+ca_crt)
err ('\t'+server_key)
err ('\t'+server_crt)
err ()

with open(config_path) as config_file:
     config=config_file.read().splitlines()

entries={}

if config_get(config, 'ssl_ca_file')!=ca_crt:
   config_set (config, 'ssl_ca_file', ca_crt)
   entries['ssl_ca_file']=ca_crt

if config_get(config, 'ssl_cert_file')!=server_crt:
   config_set (config, 'ssl_cert_file', server_crt)
   entries['ssl_cert_file']=server_crt

if config_get(config, 'ssl_key_file')!=server_key:
   config_set (config, 'ssl_key_file', server_key)
   entries['ssl_key_file']=server_key

if len(entries)>0:
   err ('The following entries need to be set in '+config_path+':')
   log_comments (log_path,
                 config_path
                )
   for entry in entries:
       err ('\t'+entry+"='"+entries[entry]+"'")
       log_comments (log_path,
                     '\t'+entry+"='"+entries[entry]+"'"
                    )

   log_comments (log_path)

   err ()
   if confirm('Do you want me to make those changes for you? [yN] ',
              default='n'
             ):
      backup_path=config_path+'.'+utc_timestamp()
      shutil.copyfile (config_path, backup_path)
      err ('Note; made a backup of the current config in:')
      err ('\t'+backup_path)
      log_comments (log_path,
                    '(changes applied;\n'+
                    ' backup in "'+backup_path+'"...)\n'+
                    '\n'
                   )

      with open(config_path,'w') as config_file:
           config_file.write ('\n'.join(config)+'\n')

# Discussions online suggest that server certificates being modified
# do not necessitate a restart with postgres 11;
# testing indicated otherwise.
# This variable is left to facilitate possible future restart logic...
restart=True

if restart:
   if confirm('Restart postgres now? [yN] ', default='n'):
      if osx():
         if 'PGDATA' in os.environ:
            pgdata=''

         elif PGDATA=='':
              pgdata=None
              err ('You will need to define PGDATA (either in the script or')
              err ('operating system environment...) to do a restart;')
              err ('postgres was not restarted. Either rerun this script')
              err ('after defining PGDATA, or restart postgres manually')

         else:
               pgdata='--pgdata="'+PGDATA+'" '

         if not (pgdata is None):
            log_comments (log_path,
                          'attempting postgres restart...'
                         )
            if shell_exit(prerequisite_command('pg_ctl')+' '+pgdata+'restart')!=ESUCCESS:
               err ('Error restarting postgres...')
               exit (errno.EHOSTDOWN) # Host is down...

            log_comments (log_path,
                          'postgres restarted...'+
                          '\n'
                         )

      elif nix():
           log_comments (log_path,
                         'attempting postgres restart...'
                        )
           if shell_exit('systemctl restart postgresql'+NO_SHELL_OUTPUT,
                         echo=False
                        )!=ESUCCESS:
              # try with version...
              version=psql_show('server_version').strip().split(' ')
              version_suffix='-'+version[0]
              if shell_exit('systemctl restart postgresql'+version_suffix+NO_SHELL_OUTPUT,
                            echo=False
                           )!=ESUCCESS:
                 err ('Error restarting postgres (using systemctl...)')
                 log_comments (log_path,
                               'Error restarting postgres (using systemctl...)'
                              )
                 exit (errno.EHOSTDOWN) # Host is down...
           else: # not shell_exit...
                 version_suffix=''

           log_comments (log_path,
                         'postgres restarted...'+
                         '\n'
                        )
           # allow time for messages toshow up in the log...
           sys.stderr.write ('Restarting postgres')
           sys.stderr.flush ()
           for second in range(RESTART_SECONDS):
               sys.stderr.write ('.')
               sys.stderr.flush ()
               time.sleep (1)

           sys.stderr.write ('\n')
           sys.stderr.flush ()

           log_comments (log_path,
                         shell_output('systemctl status postgresql'+version_suffix)+
                         '\n',
                         echo=True
                        )

      else: # Windows...
            log_comments (log_path,
                          'attempting postgres restart...'
                         )
            if shell_exit('net stop postgresql'+NO_SHELL_OUTPUT,
                          echo=False
                         )!=ESUCCESS:
               # try with version...
               version=psql_show('server_version').strip().split(' ')
               version_suffix='-'+version[0]
               if shell_exit('net stop postgresql'+version_suffix+NO_SHELL_OUTPUT,
                             echo=False
                            )!=ESUCCESS:
                  log_comments (log_path,
                                'Error restarting postgres...',
                                echo=True
                               )
                  exit (errno.EHOSTDOWN) # Host is down...

            else: # not shell_exit...
                  version_suffix=''

            sys.stderr.write ('Restarting postgres')
            sys.stderr.flush ()
            for second in range(RESTART_SECONDS):
                sys.stderr.write ('.')
                sys.stderr.flush ()
                time.sleep (1)

            sys.stderr.write ('\n')
            sys.stderr.flush ()

            if shell_exit('net start postgresql'+version_suffix,
                          echo=False
                         )!=ESUCCESS:
               log_comments (log_path,
                             'Error restarting postgres...',
                             echo=True
                            )
               exit (errno.ETIMEDOUT) # Operation timed out...

            log_comments (log_path,
                          'postgres restarted...'+
                          '\n'
                         )

err ()
err ('Now you will need to run "'+COMPANION_SCRIPT+'" for any POSTGRES users')
err ('\tyou want to grant access to.')
err ()
err ("Don't forget to configure clients to access the server by the hostname: ")
err ('\t'+hostname)
