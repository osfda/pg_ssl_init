# pg_ssl_init

A set of command line scripts that conveniently configures self-signed server and client keys and certificates to accommodate secure SSL connections to a postgres server (typically, via pgadmin...)