This is the pg_ssl_init utility package (version 11.2.7). It is a set
of command line scripts that conveniently configures self-signed server and
client keys and certificates to accommodate secure SSL connections to a postgres
server (typically, via pgadmin...)

To find out more, start by running pg_ssl_init_server, then pg_ssl_init_client,
without any arguments (you will get a "Usage" for each, with syntax...)

A SIMPLE python install script has been included (install.py) for Unix/Linux systems.
If you need one for Windows, contact the author at:

	The Open Source Financial Developers Association

	https://osfda.org/contact

This script relies on the "psql" and "openssl" utilities.

This version, although written with cross-platform capability in mind,
has not yet been _tested_ for Windows or OSx. If you encounter any
problems with its execution on those platforms, contact its author at
the OSFDA (above...)

Unix/Linux Considerations
=========================
Since configuration files that require precise permissions to be set
might need to be edited by the scripts (it always offers an option to
let you do so yourself...), and since you need to run the "psql" command
on Linux/Unix in the context of the postgres user (using "sudo"),
you will either need to have your sudoer configuration for the postgres
account correctly configured and run this script with sudo, or just run
it as root (by login or with an su...)
